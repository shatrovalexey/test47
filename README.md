### Задача ###
* описана в файле `task.pdf`.

### Технические требования ###
* `python v>=3.5`
* `MongoDB`
* дополнительные пакеты: `pip3 install pymongo tornado`

### Запуск ###
* `nohup python run.py <PORT_NUMBER> <EXPIRATION_TIME> &`
* PORT_NUMBER - номер порта нового веб-сервера (например, 8888)
* EXPIRATION_TIME - время жизни записей в секундах в `sales` (по задаче=60)
* `nohup python run.py 8888 60 &`

### Автор ###
Шатров Алексей Сергеевич <mail@ashatrov.ru>