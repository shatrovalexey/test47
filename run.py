from datetime import datetime
import tornado.ioloop
import tornado.web
import pymongo
import sys

PORT_NUMBER = sys.argv[ 1 ] # 8888
EXPIRATION_TIME = sys.argv[ 2 ] # 60

sales = pymongo.MongoClient( ).db.sales
sales.ensure_index( 'date' , expireAfterSeconds = EXPIRATION_TIME )

class Handler( tornado.web.RequestHandler ) :
	def __index( self ) :
		self.set_header( 'Content-Type' , 'text/html' )

		fh = open( 'web/index.html' , 'rb' )

		while True :
			chunk = fh.read( 0x400 * 10 )

			if not chunk : break

			self.write( chunk )

	def __statistics( self ) :
		response = { }

		for field , fn in {
			'total_sales_amount' : '$sum' ,
			'average_amount_per_order' : '$avg'
		}.items( ) :
			response[ field ] = sales.aggregate( [ {
				"$group" : {
					"_id" : None ,
					"count" : {
						fn : "$amount"
					}
				}
			} ] ).next( )[ 'count' ]

		self.write( response )

	def get( self ) :
		if self.request.path == '/' :
			self.__index( )
		else :
			self.__statistics( )

	def post( self ) :
		sales.insert_one( {
			'date' : datetime.utcnow( ) ,
			'amount' : self.get_argument( 'sales_amount' )
		} )

		response = { }

app = tornado.web.Application(
	handlers = [
		( r'/' , Handler ) ,
		( r'/sales' , Handler ) ,
		( r'/statistics' , Handler ) ,
	] ,
	autoreload = True ,
	debug = True
)
app.listen( PORT_NUMBER )

tornado.ioloop.IOLoop.current( ).start( )